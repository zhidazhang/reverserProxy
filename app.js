var http = require('http');
var httpProxy = require('http-proxy');
var config = require('./config/config.js')


var proxy = httpProxy.createProxyServer({ws:true});

proxy.on('error', function (err, req, res) {
    res.writeHead(500, {
        'Content-Type': 'text/plain'
    });
    res.end('Something went wrong. And we are reporting a custom error message.');
});

var server = require('http').createServer(function(req, res) {
    var host = req.headers.host;
    var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    console.log("client ip:" + ip + ", host:" + host);

    var length = config.web.length;
    var i=0;
    for(;i<length;i++){
        if(host==config.web[i].host){
            proxy.web(req, res, { target:config.web[i].target });
            break;
        }
    }
    if(i==length){
        res.end('not found!');
    }
});

var initProxy = function(){
    var length = config.socket.length;
    for(var i=0;i<length;i++){
       config.socket[i].proxy = new httpProxy.createProxyServer({
           target: {
               host: config.socket[i].target,
               port: config.socket[i].port
           }
       });
    }
}();


server.on('upgrade', function (req, socket, head) {
    var host = req.headers.host;
    var length = config.socket.length;
    var i=0;
    for(;i<length;i++){
        if(host==config.socket[i].host){
            config.socket[i].proxy.ws(req,socket,head);
            break;
        }
    }
});

console.log("listening on port 80")
server.listen(config.port);