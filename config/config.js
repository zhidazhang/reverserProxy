/**
 * Created by zheor on 16-9-6.
 */
module.exports ={
    web:[
       /* {
            host:'localhost:8000',
            target:'http://localhost:8080'
        },*/
        {
            host:'test.any-edu.com',
            target:'127.0.0.1:8099'
        },
        {
            host:'webchat.test.any-edu.com',
            target:'127.0.0.1:8101'
        }
    ],
    socket:[
        {
            host:'webchat.test.any-edu.com',
            target:'127.0.0.1',
            port:'8101'
        }/*
        {
            host:'localhost:8000',
            target:'localhost',
            port:'8080'
        },*/
    ],
    port:80
};

